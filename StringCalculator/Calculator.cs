﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringCalculator
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (numbers == "") return 0;

            var delimiter = numbers.Substring(2).Take(1);

            var numbersSplited = numbers.Substring(2).Split(',', '\n').Select(Int32.Parse).ToList();

            return numbersSplited.Sum();
        }
    }
}
