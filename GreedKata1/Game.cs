﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GreedKata1
{
    public class Game
    {
        private List<int> rolls;
        public Game()
        {
            this.rolls = new List<int>();
        }

        public int Play(int rounds, int number)
        {
            for (int i = 0; i < rounds; i++)
            {
                Dice dice = new Dice();
                this.rolls.Add(dice.Roll(number));
            }
            return this.CalculateScore();
        }

        public int Play(List<int> rounds)
        {
            for (int i = 0; i < rounds.Count; i++)
            {
                Dice dice = new Dice();
                this.rolls.Add(dice.Roll(rounds[i]));
            }
            return this.CalculateScore();
        }

        private int CalculateScore()
        {
            int score = 0;

            if (this.CheckRolledTriples(1))
            {
                score += 1000;
                this.RemoveRolledTriples(1);
            }

            if (CheckRolledTriples(2))
            {
                score += 200;
                RemoveRolledTriples(2);
            }

            if (CheckRolledTriples(3))
            {
                score += 300;
                RemoveRolledTriples(3);
            }

            if (CheckRolledTriples(4))
            {
                score += 400;
                RemoveRolledTriples(4);
            }

            if (CheckRolledTriples(5))
            {
                score += 500;
                RemoveRolledTriples(5);
            }

            if (CheckRolledTriples(6))
            {
                score += 600;
                RemoveRolledTriples(6);
            }

            if (this.CheckRolledOnce(1))
            {
                score += 100;
                this.RemoveRolledOnce(1);
            }

            if (this.CheckRolledOnce(5))
            {
                score += 50;
                this.RemoveRolledOnce(5);
            }

            return score;
        }

        private bool CheckRolledTriples(int number)
        {
            if (rolls.Where(x => x == number).Count() >= 3) return true;
            return false;

        }

        private bool CheckRolledOnce(int number)
        {
            if (rolls.Contains(number)) return true;
            return false;
        }

        private void RemoveRolledTriples(int number)
        {
            rolls.Remove(number);
            rolls.Remove(number);
            rolls.Remove(number);
        }
        private void RemoveRolledOnce(int number)
        {
            rolls.Remove(number);
        }
    }

}

