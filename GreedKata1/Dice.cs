﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreedKata1
{
    public class Dice
    {
        private int number;
        public int Roll()
        {
            Random rnd = new Random();
            return rnd.Next(1, 6);
        }

        public int Roll(int number)
        {
            this.number = number;
            return number;
        }
    }
}
