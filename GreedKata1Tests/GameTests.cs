﻿using GreedKata1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace GreedKata1Tests
{
    [TestClass]
    public class GameTests
    {
        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledSingleOne()
        {
            var expectedScore = 100;
            Game game = new Game();
            var result = game.Play(1, 1);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledTripleOnes()
        {
            var expectedScore = 1000;
            Game game = new Game();
            var result = game.Play(3, 1);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledSingleFive()
        {
            var expectedScore = 50;
            Game game = new Game();
            var result = game.Play(1, 5);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledTripleTwo()
        {
            var expectedScore = 200;
            Game game = new Game();
            var result = game.Play(3, 2);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledTripleThree()
        {
            var expectedScore = 300;
            Game game = new Game();
            var result = game.Play(3, 3);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledTripleFour()
        {
            var expectedScore = 400;
            Game game = new Game();
            var result = game.Play(3, 4);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledTripleFive()
        {
            var expectedScore = 500;
            Game game = new Game();
            var result = game.Play(3, 5);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_RolledTripleSix()
        {
            var expectedScore = 600;
            Game game = new Game();
            var result = game.Play(3, 6);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_ExampleSet1()
        {
            List<int> exampleSet = new List<int>() { 1, 1, 1, 5, 1 };
            int expectedScore = 1150;
            Game game = new Game();
            var result = game.Play(exampleSet);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_ExampleSet2()
        {
            List<int> exampleSet = new List<int>() { 2, 3, 4, 6, 2 };
            int expectedScore = 0;
            Game game = new Game();
            var result = game.Play(exampleSet);

            Assert.AreEqual(expectedScore, result);
        }

        [TestMethod]
        public void Should_ReturnCorrectResult_When_ExampleSet3()
        {
            List<int> exampleSet = new List<int>() { 3, 4, 5, 3, 3 };
            int expectedScore = 350;
            Game game = new Game();
            var result = game.Play(exampleSet);

            Assert.AreEqual(expectedScore, result);
        }

    }
}
