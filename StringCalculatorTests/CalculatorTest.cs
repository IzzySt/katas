﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator;
using System;
using System.Collections.Generic;
using System.Text;

namespace StringCalculatorTests
{
    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void Should_AddTwoNumbers_ReturnCorrectValue()
        {
            Calculator calculator = new Calculator();
            var result = calculator.Add("2,2");

            Assert.AreEqual(2 + 2, result);
        }

        [TestMethod]
        public void Should_Return0_WhenEmptyString()
        {
            Calculator calculator = new Calculator();
            var result = calculator.Add("");

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Should_AddOneNumber_ReturnCorrectValue()
        {
            Calculator calculator = new Calculator();
            var result = calculator.Add("2");

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void Should_AddFiewNumbers_ReturnCorrectValue()
        {
            Calculator calculator = new Calculator();
            var result = calculator.Add("2,2,2,2,2");

            Assert.AreEqual(2+ 2+ 2+ 2+ 2, result);
        }

        [TestMethod]
        public void Should_AddFiewNumbersWithDiffSeparators_ReturnCorrectValue()
        {
            Calculator calculator = new Calculator();
            var result = calculator.Add("2\n2,2,2,2");

            Assert.AreEqual(2 + 2 + 2 + 2 + 2, result);
        }
    }
}
