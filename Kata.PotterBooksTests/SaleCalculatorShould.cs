﻿using Kata.PotterBooks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kata.PotterBooksTests
{
    [TestClass]
    public class SaleCalculatorShould
    {
        private int bookPrice = 8;

        [TestMethod]
        public void ReturnCorrectPriceForOneBookOneTitle()
        {
            var expectedPrice = this.bookPrice;
            var books = new List<Book>() {new Book() {Name="Title nr 1", Price= this.bookPrice } };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);

        }

        [TestMethod]
        public void ReturnCorrectPriceForTwoBooksOneTitle()
        {
            var expectedPrice = 2 * this.bookPrice;
            var books = new List<Book>() { 
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 1", Price = this.bookPrice }};

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

        [TestMethod]
        public void ReturnCorrectPriceForTwoBooksTwoTitles()
        {
            var discount = 0.05;
            var expectedPrice = 2 * this.bookPrice - (2 * this.bookPrice * discount);
            var books = new List<Book>() {
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice }
            };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

        [TestMethod]
        public void ReturnCorrectPriceForThreeBooksTwoTitles()
        {
            var discount = 0.05;
            var expectedPrice = 3 * this.bookPrice - (2 * this.bookPrice * discount);
            var books = new List<Book>() {
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice }
            };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

        [TestMethod]
        public void ReturnCorrectPriceForFiveBooksThreeTitles()
        {
            var discount = 0.1;
            var expectedPrice = 5 * this.bookPrice - (3 * this.bookPrice * discount);
            var books = new List<Book>() {
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
            };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

        [TestMethod]
        public void ReturnCorrectPriceForFiveBooksFourTitles()
        {
            var discount = 0.15;
            var expectedPrice = 5 * this.bookPrice - (4 * this.bookPrice * discount);
            var books = new List<Book>() {
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
                new Book() { Name = "Title nr 4", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
            };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

        [TestMethod]
        public void ReturnCorrectPriceForFiveBooksFiveTitles()
        {
            var discount = 0.25;
            var expectedPrice = 5 * this.bookPrice - (5 * this.bookPrice * discount);
            var books = new List<Book>() {
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
                new Book() { Name = "Title nr 4", Price = this.bookPrice },
                new Book() { Name = "Title nr 5", Price = this.bookPrice },
            };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

        [TestMethod]
        public void ReturnCorrectPriceForSixBooksSixTitles()
        {
            var discount = 0.30;
            var expectedPrice = 6 * this.bookPrice - (6 * this.bookPrice * discount);
            var books = new List<Book>() {
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
                new Book() { Name = "Title nr 4", Price = this.bookPrice },
                new Book() { Name = "Title nr 5", Price = this.bookPrice },
                new Book() { Name = "Title nr 6", Price = this.bookPrice }
            };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

        [TestMethod]
        public void ReturnCorrectPriceForSevenBooksSevenTitles()
        {
            var discount = 0.35;
            var expectedPrice = 7 * this.bookPrice - (7 * this.bookPrice * discount);
            var books = new List<Book>() {
                new Book() { Name = "Title nr 1", Price = this.bookPrice },
                new Book() { Name = "Title nr 2", Price = this.bookPrice },
                new Book() { Name = "Title nr 3", Price = this.bookPrice },
                new Book() { Name = "Title nr 4", Price = this.bookPrice },
                new Book() { Name = "Title nr 5", Price = this.bookPrice },
                new Book() { Name = "Title nr 6", Price = this.bookPrice },
                new Book() { Name = "Title nr 7", Price = this.bookPrice }
            };

            SaleCalculator calculator = new SaleCalculator(books);
            var resultPrice = calculator.CalculatePrice();

            Assert.AreEqual(expectedPrice, resultPrice);
        }

    }
}
