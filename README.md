# TDD Kata 



## Why Kata? 
 Because for the violinist to play well and people wanted to listen to him 
            - he must practice :D
    
## Where to find examples of TDD Katas
- https://github.com/ardalis/kata-catalog
- http://codekata.com/
- https://kata-log.rocks/tdd


## Good to read
- https://github.com/dwyl/learn-tdd
- http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata
- https://blog.codinghorror.com/the-ultimate-code-kata/