﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kata.PotterBooks
{
    public class Book
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
