﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kata.PotterBooks
{
    public class SaleCalculator
    {
        private int bookPrice = 8;

        private List<Book> books;

        private Dictionary<int, double> discountDict = new Dictionary<int, double>()
        {
            [2] = 0.05,
            [3] = 0.1,
            [4] = 0.15,
            [5] = 0.25,
            [6] = 0.3,
            [7] = 0.35
        };
        public SaleCalculator(List<Book> books)
        {
            this.books = books;
        }

        public double CalculatePrice()
        {
            var price = this.books.Select(b => b.Price).Sum();
            price -= this.CalculateDiscount();
            return price;
        }

        private int CountDiffrentTitles()
        {
            return this.books.Select(b=>b.Name).Distinct().Count();
        }

        private double CalculateDiscount()
        {
            int key = CountDiffrentTitles();
            if (discountDict.ContainsKey(key))
                return key * bookPrice * discountDict[key];
            return 0;
        }
    }
}
